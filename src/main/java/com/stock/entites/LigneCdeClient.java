package com.stock.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ligneCdeClient")
public class LigneCdeClient implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idLigneCdeClient;
	
	@ManyToOne
	@JoinColumn(name = "idArticle" )
	private Article article;
	

	@ManyToOne
	@JoinColumn(name = "idCommandeClient" )
	private CommandeClient commandeClient;

	public Long getIdLigneCdeClient() {
		return idLigneCdeClient;
	}

	public void setIdLigneCdeClient(Long idLigneCdeClient) {
		this.idLigneCdeClient = idLigneCdeClient;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}
	

}
