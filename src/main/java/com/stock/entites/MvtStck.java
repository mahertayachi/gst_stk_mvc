package com.stock.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "mvtStck")
public class MvtStck implements Serializable {
	
	public static final int Entree = 1;
	public static final int Sortie = 2;

	
	@Id
	@GeneratedValue
	private Long idMvtStck;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvt;
	
	private BigDecimal quantite;

	private int typeMvt;
	
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	public Long getIdMvtStck() {
		return idMvtStck;
	}

	public void setIdMvtStck(Long idMvtStck) {
		this.idMvtStck = idMvtStck;
	}

	public Date getDateMvt() {
		return dateMvt;
	}

	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	

}
